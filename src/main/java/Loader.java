import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class Loader {

    public static List<String> loadTextFromFileToLabel() {

        List<String> listOfWords = Collections.emptyList();
        URL resource = Loader.class.getResource("/words.txt");

        try {
            Path path = Paths.get(resource.toURI());
            listOfWords = Files.readAllLines(path, Charset.forName("UTF-8"));
            return listOfWords;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfWords;
    }
}
