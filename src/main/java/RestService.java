import javax.ws.rs.*;
import java.util.Collections;
import java.util.List;

/**
 *
 */
@Path("/")
public class RestService {

    @GET
    @Path("/matchwords")
    @Produces("text/plain")
    public String matchWords() {

        List<String> textToLabel = Collections.emptyList();

        if (Loader.loadTextFromFileToLabel() != null) {
            textToLabel = Loader.loadTextFromFileToLabel();
        } else {
            return "File not found.";
        }

        return String.join(" ", textToLabel);
    }

}

